{ nightly ? true }:
let
  # Oxalica's Rust overlay gives us the rust-bin function which allows us
  # to select a specific Rust toolchain. Furthermore, we can configure
  # additional targets like shown below.
  rust_overlay = import
    (builtins.fetchTarball {
      name = "rust-overlay-2024-01-15";
      url = "https://github.com/oxalica/rust-overlay/archive/d681ac8a92a1cce066df1d3a5a7f7c909688f4be.tar.gz";
      sha256 = "1mrcbw14dlch89hnncs7pnm7lh4isqx7xywha6i97d8xs24spfvv";
    });

  # Our own overlay with additional build environment tools
  serum_overlay = import (builtins.fetchGit {
    name = "serum-overlay-2023-05-05";
    url = "https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/serum-nix-overlay.git";
    allRefs = true;
    rev = "d3781d433a1ca275c3882470dfb9b7eaeec9c0d6"; # Use the newest version here
  });

  # Pinned nixpkgs
  nixpkgsBase = import (builtins.fetchTarball {
    name = "nixpkgs-stable-23.11";
    url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/23.11.tar.gz";
    # sha256 = "0000000000000000000000000000000000000000000000000000";
    sha256 = "1ndiv385w1qyb3b18vw13991fzb9wg4cl21wglk89grsfsnra41k";
  });

  nixpkgs = nixpkgsBase {
    overlays = [ rust_overlay serum_overlay ];
  };

  rust = (
    if nightly
    then nixpkgs.rust-bin.nightly."2024-01-15"
    else nixpkgs.rust-bin.stable."1.76.0"
  ).default;

in
with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";

  buildInputs = [
    rust
    gitlab-clippy
    cargo2junit
    cargo-readme # generate README.md from lib.rs
    cargo-deny # license checking
    cargo-udeps
    cargo-tarpaulin
    nodePackages.cspell #  spell checking
    git
    which
    cacert # for certificate verification when downloading from crates.io
    libiconv # for linking on macOS
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;
}
