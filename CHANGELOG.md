# Changelog

# v1.1.0
Add `TryAscii` trait which adds `try_ascii` method to `[u8]`, `&[u8]` and `dyn AsRef<[u8]>`

# v1.0.0
First public version on crates.io
