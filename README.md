# Try ASCII

Helper to format byte slices that probably/mostly contain ASCII-encoded text

This can be used to log binary data which is expected to be text but which is not guaranteed to be
valid UTF-8. In these cases, it can be unfeasible to use
[`std::str::from_utf8`](https://doc.rust-lang.org/std/str/fn.from_utf8.html)
because whenever that function fails, an `Utf8Error` would be logged instead of the actual data.
Using `try_ascii`, the text parts are formatted as text while the binary parts fall back to the
default `Debug` implementation for `&[u8]`:

```rust
use try_ascii::try_ascii;
println!("{:x?}", try_ascii(b"Hello, \xa0\xa1\xa2\xa3"));
```

Prints as:
> Hello, [a0, a1, a2, a3]

You can either use the provided `try_ascii` function, or import the `TryToAscii` trait,
that automatically implements `try_ascii()` for `[u8]`, `&[u8]` and everything that implements `AsRef<[u8]>`:

```rust
use try_ascii::TryAscii;

// You can use .try_ascii() directly on a [u8]-slice:
println!("{:x?}", b"Hello, \xa0\xa1\xa2\xa3".try_ascii());

// ... or on &[u8] :
let slice: &[u8] = b"Hello, \xa0\xa1\xa2\xa3";
println!("{:x?}", slice.try_ascii());

// ... or on things that implement AsRef<[u8]>:
let slice: [u8; 11] = *b"Hello, \xa0\xa1\xa2\xa3";
println!("{:x?}", slice.as_ref().try_ascii());
```

## License

Open Logistics Foundation License\
Version 1.3, January 2023

See the LICENSE file in the top-level directory.

## Contact
Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>
